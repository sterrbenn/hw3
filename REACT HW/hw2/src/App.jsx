import React, { useState, useEffect } from 'react';
import ProductsList from './components/ProductList/ProductsList.jsx';
import Header from './components/Header/Header.jsx';
import MainImg from './components/MainImg/MainImg.jsx';
import Overlay from './components/Overlay/Overlay.jsx';

function App() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [cartCount, setCartCount] = useState(0);
  const [favorite, setFavorite] = useState (0);

  function openModal() {
    setIsModalOpen(true);
  };

  function closeModal() {
    setIsModalOpen(false);
  };


  useEffect(() => {
    localStorage.setItem('cartCount', cartCount);
  }, [cartCount]);

 
  return (
    <div className="App">
      <Header cartCount={cartCount} favorite={favorite}/>
      {isModalOpen && <Overlay onClick={closeModal} />}
      <MainImg />
      <ProductsList openModal={openModal} closeModal={closeModal} setCartCount={setCartCount} setFavorite ={setFavorite}/>
    </div>
  );
}

export default App;
