import './Button.css'

function Button ({ type = "button", classNames = "", onClick, children, }){
    return(

        <button type={type} onClick={onClick} className='openModalBtn'>{children}</button>
    )
}

export default Button;