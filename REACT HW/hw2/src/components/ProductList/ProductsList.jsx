import React, { useEffect, useState } from 'react';
import ProductCard from '../ProductCard/ProductCard.jsx';
import '../ProductList/ProductList.css';

function ProductsList ({setCartCount, setFavorite, favorite}) {
  const [products, setProducts] = useState([]);

const fetchProducts = async function () {
    try {
      const response = await fetch('/products.json');
      const data = await response.json();
      setProducts(data.products);
    } catch (error) {
      console.error('Error fetching data', error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div>
      <h1 className='title'>Товари</h1>
      <ul className="products-list">
        {products.map((product, index) => (
         <ProductCard key={index} product={product} setCartCount={setCartCount} setFavorite={setFavorite} favorite={favorite} />
        ))}
      </ul>
    </div>
  );
};

export default ProductsList;