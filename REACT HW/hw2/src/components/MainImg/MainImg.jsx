import Img from '../../assets/adv1.jpg'
import './MainImg.css'

function MainImg () {
    return (
        <>
        <div>
        <img className='MainImg' src={Img} alt="phone"/>
        </div>
        </>
    )
}

export default MainImg;