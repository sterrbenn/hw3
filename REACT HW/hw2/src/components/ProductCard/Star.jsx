import './Star.css'
import React, { useState } from 'react';

function Star({ setFavorite, product }) {

  const [isFilled, setIsFilled] = useState(false);

  function handleClick() {

    setIsFilled(!isFilled);

    if (isFilled) {
      setFavorite(prevFavorite => prevFavorite + 1);
    } else {
      setFavorite(prevFavorite => prevFavorite - 1);
    }
    setIsFilled(!isFilled);

    const favoriteCard = {
      id: product.sku,
      name: product.name,
      price: product.price,
      imageUrl: product.imageUrl,
      vendorCode: product.vendorCode,
      color: product.color
    };

    const favoriteList = JSON.parse(localStorage.getItem('favoriteList'))

    favoriteList.push(favoriteCard);

    localStorage.setItem('favoriteList', JSON.stringify(favoriteList));

    console.log(favoriteCard);
  }


  return (
    <svg
      onClick={handleClick}
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill={isFilled ? "gold" : "none"}
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className="star"
      style={{ cursor: 'pointer' }}
    >
      <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2" />
    </svg>
  );
}


export default Star