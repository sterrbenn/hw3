import './Header.css'
import ShopLogo from "../../assets/allo.png"
import Cart from "../../assets/cart.svg"
import Heart from "../../assets/heart.svg"
import React, {useState, useEffect} from 'react'
import HeaderCounter from './HeaderCounter'
import FavoriteCounter from './FavoriteCounter'

function Header({cartCount, favorite}) {
    
    return (
        <>
            <section className='header'>
                <img className="header__logo" src={ShopLogo} alt="allo" />
                <nav className='header__menu'>
                    <ul className='header__btns'>
                        <li><a href="#">Дізнатися більше</a></li>
                        <li><a href="#">Доставка</a></li>
                        <li><a href="#">Зв'язатися з нами</a></li>
                        <li><a href="#">Адреси магазинів</a></li>
                        <li><a href="#">Акції %</a></li>
                    </ul>
                    <div className='searchCont'>
                        <input type="text" className='search' placeholder='Я шукаю...' />
                        <button className="search-button">🔍</button>
                    </div>
                    <img className='header__cart' src={Cart} alt="cart" />
    
                    <HeaderCounter cartCount={cartCount}/>
                    <img className='favorite' src={Heart} alt="Heart" />
                    <FavoriteCounter favorite={favorite}/>

                </nav>
            </section>
        </>
    )
}


export default Header
