import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import Button from '../components/Button/Button'
import store from '../components/store';

test('Модальне вікно у кошику відкривається', () => {
  <Provider store={store}>
    render(<Button />);
    const btn = screen.getByRole('button');
    fireEvent.click(btn);
    const modalCart = screen.getByTestId('modalCart');
    expect(modalCart).toBeInTheDocument();
  </Provider>
});