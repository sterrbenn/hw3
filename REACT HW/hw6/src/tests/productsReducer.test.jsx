import store, { setProducts,  openModal, closeModal } from '../components/store';

test('Отримання товарів відбувається корректно', () => {
    const initialState = { products: [] };
    const products = [{ id: 1, name: 'Product 1' }, { id: 2, name: 'Product 2' }];
    store.dispatch(setProducts(products));
    const newState = store.getState().products;
    expect(newState.products).toEqual(products);
});

test('модальне вікно відкривається', () => {
    const initialState = { isModalOpen: false };
    store.dispatch(openModal());
    const newState = store.getState().modal;
    expect(newState.isModalOpen).toBe(true);
});

test('модальне вікно закривається', () => {
    const initialState = { isModalOpen: true };
    store.dispatch(closeModal());
    const newState = store.getState().modal;
    expect(newState.isModalOpen).toBe(false);
});
