import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Button from '../components/Button/Button';

test('Кнопка має відповідний текст', () => {
  render(<Button>Додати до кошика</Button>);
  const button = screen.getByText('Додати до кошика');
  expect(button).toBeInTheDocument();
});

test('Кнопка має відповідний текст', () => {
  render(<Button>Додати до кошика</Button>);
  const button = screen.getByText('Додати до кошика');
  expect(button).toMatchSnapshot();
});
