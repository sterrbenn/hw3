import React, { createContext, useState, useContext } from 'react';

const ViewTypeContext = createContext();

export const ViewTypeProvider = ({ children }) => {
  const [viewType, setViewType] = useState('cards');

  const toggleViewType = () => {
    setViewType(prevType => prevType === 'cards' ? 'table' : 'cards');
  };

  return (
    <ViewTypeContext.Provider value={{ viewType, toggleViewType }}>
      {children}
    </ViewTypeContext.Provider>
  );
};

export const useViewType = () => useContext(ViewTypeContext);
