import React, { useState, useEffect } from 'react';
import ProductCard from '../ProductCard/ProductCard';
import DeleteFromCartBtn from '../Button/DeleteFromCartBtn';
import './Favorite.css'

function Favorite({ setCartCount, setFavorite }) {
  const [favoriteItems, setFavoriteItems] = useState([]);

  useEffect(() => {
    const favoriteItemsFromStorage = JSON.parse(localStorage.getItem('favoriteList'));
    setFavoriteItems(favoriteItemsFromStorage);
  }, []);

  const removeFromFavorite = (index) => {
    const updatedFavoriteItems = [...favoriteItems.slice(0, index), ...favoriteItems.slice(index + 1)];
    localStorage.setItem('favoriteList', JSON.stringify(updatedFavoriteItems));
    setFavoriteItems(updatedFavoriteItems);
  };

  return (
    <div className='favoriteContent'>
      {favoriteItems && favoriteItems.length > 0 ? (
        favoriteItems.map((product, index) => (
          <div key={index}>
            <DeleteFromCartBtn onClick={() => removeFromFavorite(index)} />
            <ProductCard
              product={product}
              setCartCount={setCartCount}
              setFavorite={setFavorite}
              buttonText="Перейти до замовлення"
              modalContent={
                <>
                  <h2>Додати {product.name} до кошика?</h2>
                  <p>Ціна: ${product.price}</p>
                </>
              }
            />
          </div>
        ))
      ) : (
        <p>Немає улюблених товарів.</p>
      )}
    </div>
  );
}

export default Favorite;
