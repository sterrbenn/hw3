import React, { useState } from 'react';
import Button from '../Button/Button.jsx';
import { useDispatch } from 'react-redux';
import { openModal, closeModal } from '../store';
import { useOutletContext } from 'react-router-dom';
import { Modal, ModalBody, ModalFooter, ModalClose } from '../Modal/Modal.jsx';

function ProductTable({ products, setCartCount, setFavorite, favorite }) {
  const dispatch = useDispatch();
  const { setCartItems, cartItems } = useOutletContext();

  const [modalOpenIndex, setModalOpenIndex] = useState(-1);

  const handleOpenModal = (index) => {
    setModalOpenIndex(index); 
    dispatch(openModal());
  };

  const handleCloseModal = () => {
    setModalOpenIndex(-1); 
    dispatch(closeModal());
  };

  const handleAddToCart = (product) => {
    handleCloseModal();

    const cartItem = {
      id: product.sku,
      name: product.name,
      price: product.price,
      imageUrl: product.imageUrl,
      vendorCode: product.vendorCode,
      color: product.color
    };

    const updatedCartItems = [...cartItems, cartItem];
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    setCartItems(updatedCartItems);
  };

  return (
    <table className="products-table">
      <thead>
        <tr>
          <th>Фото</th>
          <th>Назва</th>
          <th>Ціна</th>
          <th>Колір</th>
          <th>Артикул</th>
     
        </tr>
      </thead>
      <tbody>
        {products.map((product, index) => (
          <tr key={index}>
            <td><img src={product.imageUrl} alt={product.name} className="product-image" /></td>
            <td className="product-name">{product.name}</td>
            <td className="product-price">${product.price}</td>
            <td className="product-color">{product.color}</td>
            <td className="product-vendorCode">{product.vendorCode}</td>
            <td>
              <Button type="button" className="btn-secondary" onClick={() => handleOpenModal(index)}>
                Додати до кошика
              </Button>
              {modalOpenIndex === index && (
                <Modal>
                  <ModalBody>
                    <>
                      <h2>Додати {product.name} до кошика?</h2>
                      <p>Ціна: ${product.price}</p>
                    </>
                  </ModalBody>
                  <ModalFooter
                    firstText="Так"
                    firstClick={() => handleAddToCart(product)}
                    secondaryText="Ні"
                    secondaryClick={handleCloseModal}
                  />
                  <ModalClose onClick={handleCloseModal} />
                </Modal>
              )}
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default ProductTable;
