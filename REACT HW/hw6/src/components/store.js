import { configureStore, createSlice } from '@reduxjs/toolkit';

export const initialProductsState = {
  products: []
};

const initialModalState = {
  isModalOpen: false
};

const productsSlice = createSlice({
  name: 'products',
  initialState: initialProductsState,
  reducers: {
    setProducts(state, action) {
      state.products = action.payload;
    }
  }
});

const modalSlice = createSlice({
  name: 'modal',
  initialState: initialModalState,
  reducers: {
    openModal(state) {
      state.isModalOpen = true;
    },
    closeModal(state) {
      state.isModalOpen = false;
    }
  }
});

export const { setProducts } = productsSlice.actions;
export const { openModal, closeModal } = modalSlice.actions;

const store = configureStore({
  reducer: {
    products: productsSlice.reducer,
    modal: modalSlice.reducer
  }
});

export default store;
export { modalSlice };
