import FooterImg from '../../assets/arr.svg'
import './Footer.css'

function Footer () {
    return (
        <div className='footer'>
            <span className='footerText'>Dnipro, Ukraine 2024</span>
            <img className='footerImg' src={FooterImg} alt="All rights Reserved" />
        </div>
    )
}

export default Footer