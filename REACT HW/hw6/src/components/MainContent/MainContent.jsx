import React from 'react';
import MainImg from '../MainImg/MainImg.jsx';
import ProductsList from '../ProductList/ProductsList.jsx';
import Overlay from '../Overlay/Overlay.jsx';
import { ViewTypeProvider } from '../ViewTypeContext';

function MainContent({ isModalOpen, closeModal, openModal, setCartCount, setFavorite }) {
  return (
    <>
      {isModalOpen && <Overlay onClick={closeModal} />}
      <MainImg />
      <ViewTypeProvider> 
        <ProductsList openModal={openModal} closeModal={closeModal} setCartCount={setCartCount} setFavorite={setFavorite} />
      </ViewTypeProvider>
    </>
  );
}

export default MainContent;
