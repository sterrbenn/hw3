import './Button.css'
import React from 'react';

function Button ({ type = "button", onClick, children, }){
    return(

        <button type={type} onClick={onClick} className='openModalBtn'>{children}</button>
    )
}

export default Button;