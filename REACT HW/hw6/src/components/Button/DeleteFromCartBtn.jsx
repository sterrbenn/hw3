import './DeleteFromCartBtn.css'

function DeleteFromCartBtn({onClick}) {
    return(
      <button className="cross" onClick={onClick}>×</button>
    )
}

export default DeleteFromCartBtn