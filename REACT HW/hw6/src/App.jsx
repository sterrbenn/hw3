import React from 'react';
import MainContent from './components/MainContent/MainContent.jsx'; 

function App({ isModalOpen, closeModal, openModal, setCartCount, setFavorite }) {
  return (
    <div className="App">
      <MainContent
        isModalOpen={isModalOpen}
        closeModal={closeModal}
        openModal={openModal}
        setCartCount={setCartCount}
        setFavorite={setFavorite}
      />
    </div>
  );
}

export default App;
