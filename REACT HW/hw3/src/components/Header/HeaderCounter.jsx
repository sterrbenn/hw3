
function HeaderCounter({ cartCount }) {

    return (
        <span className="cart__counter">{cartCount}</span>
    )
}

export default HeaderCounter