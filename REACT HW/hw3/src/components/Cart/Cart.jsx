import React, { useState, useEffect } from 'react';
import ProductCard from '../ProductCard/ProductCard';
import './Cart.css';
import DeleteFromCartBtn from '../Button/DeleteFromCartBtn';

function Cart({ setCartCount, setFavorite }) {
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems'));
    setCartItems(cartItemsFromStorage);
  }, []);

  const removeFromCart = (index) => {
    const updatedCartItems = [...cartItems.slice(0, index), ...cartItems.slice(index + 1)];
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    setCartItems(updatedCartItems);
  };

  return (
    <div className='cartContent'>
      {cartItems.map((product, index) => (
        <div key={index}>
          <DeleteFromCartBtn onClick={() => removeFromCart(index)} />
          <ProductCard
            product={product}
            setCartCount={setCartCount}
            setFavorite={setFavorite}
            buttonText="Перейти до замовлення"
            modalContent={
              <>
                <h2>Перейти до оформлення товару {product.name}?</h2>
                <p>Ціна: ${product.price}</p>
              </>
            }
          />
        </div>
      ))}
    </div>
  );
}

export default Cart;
