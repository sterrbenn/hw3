import React, { useEffect, useState } from 'react';
import { useOutletContext } from 'react-router-dom';
import ProductCard from '../ProductCard/ProductCard.jsx';
import './ProductList.css';

function ProductsList( {modalContent}) {
  const { setCartCount, setFavorite, favorite } = useOutletContext();
  const [products, setProducts] = useState([]);

  const fetchProducts = async () => {
    try {
      const response = await fetch('/products.json');
      const data = await response.json();
      setProducts(data.products);
    } catch (error) {
      console.error('Error fetching data', error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  return (
    <div>
      <h1 className='title'>Товари</h1>
      <ul className="products-list">
        {products.map((product, index) => (
          <ProductCard 
            key={index} 
            product={product} 
            setCartCount={setCartCount} 
            setFavorite={setFavorite} 
            favorite={favorite} 
            buttonText={'Додати до кошика'}
            modalContent={
              <>
                <h2>Додати {product.name} до кошика?</h2>
                <p>Ціна: ${product.price}</p>
              </>} 
          />
        ))}
      </ul>
    </div>
  );
}

export default ProductsList;
