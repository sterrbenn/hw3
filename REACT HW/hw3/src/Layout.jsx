import React, { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";


function Layout() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [cartCount, setCartCount] = useState(0);
  const [favorite, setFavorite] = useState(0);

  function openModal() {
    setIsModalOpen(true);
  }

  function closeModal() {
    setIsModalOpen(false);
  }

  useEffect(() => {
    localStorage.setItem('cartCount', cartCount);
  }, [cartCount]);

  return (
    <div>
      <Header cartCount={cartCount} favorite={favorite} />
      <div className="main-content">
        <Outlet
          context={{
            isModalOpen,
            closeModal,
            openModal,
            setCartCount,
            setFavorite,
          }}
        />
      </div>
      <Footer/>
    </div>
  );
}

export default Layout;
