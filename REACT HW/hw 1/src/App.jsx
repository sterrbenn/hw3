import React, { useState } from 'react';
import Button from "./components/Button/Button.jsx";
import { Modal, ModalWrapper, ModalHeader, ModalFooter, ModalClose, ModalBody } from "./components/Modal/Modal.jsx";
import Image from "./assets/img.png"
import Overlay from "./components/Overlay/Overlay.jsx";

function App() {
  const [firstModalOpen, setFirstModalOpen] = useState(false);
  const [secondModalOpen, setSecondModalOpen] = useState(false);

  const openFirstModal = () => {
    setFirstModalOpen(true);
  };

  const closeFirstModal = () => {
    setFirstModalOpen(false);
  };

  const openSecondModal = () => {
    setSecondModalOpen(true);
  };

  const closeSecondModal = () => {
    setSecondModalOpen(false);
  };

  return (
    <>

<div className="openModalCont">
        <Button type="button" className="btn-primary" onClick={openFirstModal}>
          Open first modal
        </Button>
        <Button type="button" className="btn-secondary" onClick={openSecondModal}>
          Open second modal
        </Button>
      </div>

      {firstModalOpen && <Overlay onClick={closeFirstModal} />}
      {secondModalOpen && <Overlay onClick={closeSecondModal} />}

      <div className='modalFlexCont'>
        {firstModalOpen && (
          <Modal>
            <ModalWrapper>
              <ModalHeader>
                <img className='modalImg' src={Image} alt="img" />
                <ModalClose onClick={closeFirstModal} />
              </ModalHeader>

              <ModalBody

                mainText="Product Delete!"
                secondaryText="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.">

              </ModalBody>

              <ModalFooter
                firstText="NO, CANCEL"
                firstClick={closeFirstModal}
                secondaryText='YES, DELETE'
              />
            </ModalWrapper>
          </Modal>
        )}

        {secondModalOpen && (
          <Modal>
            <ModalWrapper>
              <ModalHeader>
                <ModalClose onClick={closeSecondModal} />
              </ModalHeader>

              <ModalBody
                mainText="Add Product “NAME”"
                secondaryText="Description for you product">
              </ModalBody>

              <ModalFooter
                firstText="ADD TO FAVORITE"
                firstClick={closeSecondModal}
              />
            </ModalWrapper>
          </Modal>
        )}
      </div>
    </>
  );
}

export default App;