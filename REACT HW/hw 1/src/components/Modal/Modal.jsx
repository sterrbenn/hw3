import Cross from '../../assets/cross.png'
import './Modal.css'
import './ModalClose.css'
import './ModalImg.css'
import './ModalWrapper.css'
import './ModalBody.css'
import './ModalFooter.css'
import './ModalSubmitBtn.css'
import '../Button/DeclineBtn.css'
import './ModalHeader.css'
import '../Overlay/Overlay.css'

function ModalWrapper({ children }) {
  return (
    <div className="modal-wrapper">
      {children}
    </div>
  );
};

function ModalHeader({ children }) {
  
  return (
    <div className="modal-header">
      {children}
    </div>
  );
};

function ModalFooter({ firstText, secondaryText, firstClick, secondaryClick }) {
  return (
    <div className="modal-footer">
      <div className='btns__cont'>
        {firstText && <button className='submitBtn' onClick={firstClick}>{firstText}</button>}
        {secondaryText && <button className='declineBtn' onClick={firstClick}>{secondaryText}</button>}
      </div>
    </div>
  );
};

function ModalClose({ onClick }) {
  return (
    <button className="modal-close" onClick={onClick}>
      <img className='modal-cross' src={Cross} alt="Close" />
    </button>
  );
};

function ModalBody({ children, mainText, secondaryText }) {
  return (
    <div className="modal-body">
      {children}
      <h2 className='card__h2'>{mainText}</h2>
      <span className='card__span'>{secondaryText}</span>
    </div>
  );
};

function Modal({ children }) {
  return (
    <div className="modal">
      {children}
    </div>
  );
};



export { ModalWrapper, ModalHeader, ModalFooter, ModalClose, ModalBody, Modal };