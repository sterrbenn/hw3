import React, { useState, useEffect } from 'react';
import ProductCard from '../ProductCard/ProductCard';
import './Cart.css';
import DeleteFromCartBtn from '../Button/DeleteFromCartBtn';

function Cart({ setCartCount, setFavorite }) {
  const [cartItems, setCartItems] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentProduct, setCurrentProduct] = useState(null);

  useEffect(() => {
    const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems'));
    setCartItems(cartItemsFromStorage);
  }, []);

  const removeFromCart = (index) => {
    const updatedCartItems = [...cartItems.slice(0, index), ...cartItems.slice(index + 1)];
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    setCartItems(updatedCartItems);
  };

   const handleOpenModal = (product) => {
    setCurrentProduct(product);
    setIsModalOpen(true);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
    setCurrentProduct(null);
  };

  return (
    <div className='cartContent'>
      {cartItems && cartItems.length > 0 ? (
        cartItems.map((product, index) => (
          <div key={index}>
            <DeleteFromCartBtn onClick={() => removeFromCart(index)} />
            <ProductCard
              product={product}
              setCartCount={setCartCount}
              setFavorite={setFavorite}
              buttonText="Перейти до замовлення"
              onButtonClick={() => handleOpenModal(product)}
            />
          </div>
        ))
      ) : (
        <p>Немає доданих до кошику товарів</p>
      )}

      {isModalOpen && (
        <ModalCart />
      )}
    </div>
  );
}

export default Cart;
