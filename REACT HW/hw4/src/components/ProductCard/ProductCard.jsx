import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import './ProductCard.css';
import Button from '../Button/Button.jsx';
import { Modal, ModalBody, ModalFooter, ModalClose } from '../Modal/Modal.jsx';
import Star from './Star.jsx';
import { openModal, closeModal } from '../store';

function ProductCard({ product, setCartCount, setFavorite, favorite, buttonText, modalContent, onButtonClick  }) {
  const dispatch = useDispatch();
  const [isLocalModalOpen, setIsLocalModalOpen] = useState(false);

  function handleOpenModal() {
    setIsLocalModalOpen(true);
    dispatch(openModal());
  }

  function handleCloseModal() {
    setIsLocalModalOpen(false);
    dispatch(closeModal());
  }

  function handleAddToCart() {
    setCartCount(prevCount => prevCount + 1);
    handleCloseModal();

    const cartItem = {
      id: product.sku,
      name: product.name,
      price: product.price,
      imageUrl: product.imageUrl,
      vendorCode: product.vendorCode,
      color: product.color
    };

    const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems')) || [];
    const updatedCartItems = [...cartItemsFromStorage, cartItem];
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    console.log(cartItem);
  }

  return (
    <div className="product-card">
      <img src={product.imageUrl} alt={product.name} className="product-image" />
      <h2 className="product-name">{product.name}</h2>
      <p className="product-price">Ціна: ${product.price}</p>
      <p className="product-color">Колір: {product.color}</p>
      <p className="product-vendorCode">Артикул: {product.vendorCode}</p>
      <Button type="button" className="btn-secondary" onClick={handleOpenModal}>
        {buttonText}
      </Button>
      <Star favorite={favorite} setFavorite={setFavorite} product={product} />
      {isLocalModalOpen && (
        <Modal>
          <ModalBody>
            {modalContent}
          </ModalBody>
          <ModalFooter
            firstText="Так"
            firstClick={handleAddToCart}
            secondaryText="Ні"
            secondaryClick={handleCloseModal}
          />
          <ModalClose />
        </Modal>
      )}
    </div>
  );
}

export default ProductCard;
