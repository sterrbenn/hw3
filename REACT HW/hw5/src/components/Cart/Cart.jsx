import React, { useState, useEffect } from 'react';
import ProductCard from '../ProductCard/ProductCard';
import './Cart.css';
import DeleteFromCartBtn from '../Button/DeleteFromCartBtn';
import Button from '../Button/Button';
import ModalCart from '../Modal/ModalCart';
import { useOutletContext } from 'react-router-dom';

function Cart({ setFavorite }) {
  const [cartItems, setCartItems] = useState([]);
  const [isModalOpen, setModalOpen] = useState(false);
  const { setCartItems: setGlobalCartItems, cartItems: globalCartItems } = useOutletContext();

  const openModal = () => {
    setModalOpen(true);
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  useEffect(() => {
    const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems')) || [];
    setCartItems(cartItemsFromStorage);
    setGlobalCartItems(cartItemsFromStorage);
  }, [setGlobalCartItems]);

  const removeFromCart = (index) => {
    const updatedCartItems = [...cartItems.slice(0, index), ...cartItems.slice(index + 1)];
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    setCartItems(updatedCartItems);
    setGlobalCartItems(updatedCartItems);
  };

  return (
    <div>
      <div className='cartContent'>
        {cartItems.length > 0 ? (
          cartItems.map((product, index) => (
            <div key={index}>
              <DeleteFromCartBtn onClick={() => removeFromCart(index)} />
              <ProductCard
                product={product}
                setFavorite={setFavorite}
                hideButton={true}
                buttonText="Перейти до замовлення"
                modalContent={
                  <>
                    <h2>Перейти до оформлення товару {product.name}?</h2>
                    <p>Ціна: ${product.price}</p>
                  </>
                }
              />
            </div>
          ))
        ) : (
          <p>Немає доданих до кошику товарів.</p>
        )}
      </div>

      <div className='go-to-order'>
        <Button onClick={openModal}>Перейти до замовлення</Button>
      </div>
      {isModalOpen && <ModalCart closeModal={closeModal} />}
    </div>
  );
}

export default Cart;
