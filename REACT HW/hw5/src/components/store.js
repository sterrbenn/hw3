import { configureStore, createSlice } from '@reduxjs/toolkit';

const initialProductsState = {
  products: []
};

const initialModalState = {
  isModalOpen: false
};

const productsSlice = createSlice({
  name: 'products',
  initialState: initialProductsState,
  reducers: {
    setProducts(state, action) {
      state.products = action.payload;
    }
  }
});

const modalSlice = createSlice({
  name: 'modal',
  initialState: initialModalState,
  reducers: {
    openModal(state) {
      state.isModalOpen = true;
    },
    closeModal(state) {
      state.isModalOpen = false;
    }
  }
});

// export const fetchProducts = createAsyncThunk(
//     'products/fetchProducts',
//     async () => {
//       try {
//         const response = await fetch('/products.json');
//         const data = await response.json();
//         return data.products;
//       } catch (error) {
//         throw Error('Error fetching products');
//       }
//     }
//   );

// Замість звичайної асинхронної функції намагався використати createAsyncThunk. 
// Вона має використовуватися у ProductList, але якщо я імплементую її замість звичайної асинхронної функції, список перестає відмальовуватися.
// При цьому немає жодних помилок у консолі, компонент ProductList бачить цю функцію, вона не null/undefined, але нічого не працює.



export const { setProducts } = productsSlice.actions;
export const { openModal, closeModal } = modalSlice.actions;

const store = configureStore({
  reducer: {
    products: productsSlice.reducer,
    modal: modalSlice.reducer
  }
});

export default store;
