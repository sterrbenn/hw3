import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useOutletContext } from 'react-router-dom';
import ProductCard from '../ProductCard/ProductCard.jsx';
import { setProducts } from '../store.js';
import './ProductList.css';

function ProductsList() {
  const { setCartCount, setFavorite, favorite } = useOutletContext();
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.products);

  const fetchProducts = async () => {
    try {
      const response = await fetch('/products.json');
      const data = await response.json();
      dispatch(setProducts(data.products));
    } catch (error) {
      console.error('Error fetching data', error);
    }
  };

  useEffect(() => {
    fetchProducts();
  }, []);



  return (
    <div>
      <h1 className='title'>Товари</h1>
      <ul className="products-list">
        {products.map((product, index) => (
          <ProductCard 
            key={index} 
            product={product} 
            setCartCount={setCartCount} 
            setFavorite={setFavorite} 
            favorite={favorite} 
            buttonText={'Додати до кошика'}
            modalContent={
              <>
                <h2>Додати {product.name} до кошика?</h2>
                <p>Ціна: ${product.price}</p>
              </>} 
          />
        ))}
      </ul>
    </div>
  );
}

export default ProductsList;
