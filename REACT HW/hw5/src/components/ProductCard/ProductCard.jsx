import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import './ProductCard.css';
import Button from '../Button/Button.jsx';
import { Modal, ModalBody, ModalFooter, ModalClose } from '../Modal/Modal.jsx';
import Star from './Star.jsx';
import { openModal, closeModal } from '../store';
import { useOutletContext } from 'react-router-dom';

function ProductCard({ product, setFavorite, favorite, buttonText, modalContent, hideButton }) {
  const dispatch = useDispatch();
  const { setCartItems, cartItems } = useOutletContext();
  const [isLocalModalOpen, setIsLocalModalOpen] = useState(false);

  function handleOpenModal() {
    setIsLocalModalOpen(true);
    dispatch(openModal());
  }

  function handleCloseModal() {
    setIsLocalModalOpen(false);
    dispatch(closeModal());
  }

  function handleAddToCart() {
    handleCloseModal();

    const cartItem = {
      id: product.sku,
      name: product.name,
      price: product.price,
      imageUrl: product.imageUrl,
      vendorCode: product.vendorCode,
      color: product.color
    };

    const updatedCartItems = [...cartItems, cartItem];
    localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    setCartItems(updatedCartItems);
  }

  return (
    <div className="product-card">
      <img src={product.imageUrl} alt={product.name} className="product-image" />
      <h2 className="product-name">{product.name}</h2>
      <p className="product-price">Ціна: ${product.price}</p>
      <p className="product-color">Колір: {product.color}</p>
      <p className="product-vendorCode">Артикул: {product.vendorCode}</p>
      {!hideButton && (
        <Button type="button" className="btn-secondary" onClick={handleOpenModal}>
          {buttonText}
        </Button>
      )}
      <Star favorite={favorite} setFavorite={setFavorite} product={product} />
      {isLocalModalOpen && (
        <Modal>
          <ModalBody>
            {modalContent}
          </ModalBody>
          <ModalFooter
            firstText="Так"
            firstClick={handleAddToCart}
            secondaryText="Ні"
            secondaryClick={handleCloseModal}
          />
          <ModalClose />
        </Modal>
      )}
    </div>
  );
}

export default ProductCard;
