import './Star.css'
import React, { useState, useEffect } from 'react';

function Star({ product, setFavorite }) {
  const [isFilled, setIsFilled] = useState(false);

  useEffect(() => {
    const favoriteList = JSON.parse(localStorage.getItem('favoriteList')) || [];
    setIsFilled(favoriteList.some(item => item.id === product.id));
  }, [product.id]);

  const handleClick = () => {
    const favoriteList = JSON.parse(localStorage.getItem('favoriteList')) || [];
    let updatedFavoriteList;

    if (isFilled) {
      updatedFavoriteList = favoriteList.filter(item => item.id !== product.id);
      setFavorite(prevFavorite => prevFavorite - 1);
    } else {
      updatedFavoriteList = [...favoriteList, product];
      setFavorite(prevFavorite => prevFavorite + 1);
    }

    localStorage.setItem('favoriteList', JSON.stringify(updatedFavoriteList));
    setIsFilled(!isFilled);
  };

  return (
    <svg
      onClick={handleClick}
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill={isFilled ? "gold" : "none"}
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
      className="star"
      style={{ cursor: 'pointer' }}
    >
      <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2" />
    </svg>
  );
}

export default Star;
