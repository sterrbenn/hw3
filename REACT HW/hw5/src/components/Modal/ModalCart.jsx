import React from 'react';
import { useDispatch } from 'react-redux';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import './ModalCart.css';

function ModalCart() {
    const dispatch = useDispatch();

    const initialValues = {
        firstName: '',
        lastName: '',
        age: '',
        address: '',
        phone: '',
    };

    const validationSchema = Yup.object().shape({
        firstName: Yup.string().required("Ім'я обов'язкове"),
        lastName: Yup.string().required("Прізвище обов'язкове"),
        age: Yup.number().required("Вік обов'язковий").typeError("Вік має бути числом"),
        address: Yup.string().required("Адреса обов'язкова"),
        phone: Yup.string().matches(/^\d+$/, "Телефон має містити лише цифри").required("Телефон обов'язковий"),
    });

    const handleSubmit = (values, { resetForm }) => {
        const age = parseInt(values.age, 10);
        if (isNaN(age)) {
            console.error('Invalid age');
            return;
        }

        const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];

        console.log('Form Data Submitted:', values);
        console.log('Purchased Items:', cartItems);

     
        resetForm();
    };

    return (
        <div className="form-container">
            <h2>Оформити замовлення</h2>
            <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={handleSubmit}
            >
                {() => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="firstName">Ім'я</label>
                            <Field type="text" id="firstName" name="firstName" />
                            <ErrorMessage name="firstName" component="div" className="error" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="lastName">Прізвище</label>
                            <Field type="text" id="lastName" name="lastName" />
                            <ErrorMessage name="lastName" component="div" className="error" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="age">Вік</label>
                            <Field type="text" id="age" name="age" />
                            <ErrorMessage name="age" component="div" className="error" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="address">Адреса доставки</label>
                            <Field type="text" id="address" name="address" />
                            <ErrorMessage name="address" component="div" className="error" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="phone">Мобільний телефон</label>
                            <Field type="tel" id="phone" name="phone" />
                            <ErrorMessage name="phone" component="div" className="error" />
                        </div>
                        <button type="submit">Готово!</button>
                    </Form>
                )}
            </Formik>
        </div>
    );
}

export default ModalCart;
