
function FavoriteCounter({ favorite }) {

    return (
        <span className="favorite__counter">{favorite}</span>
    )
}

export default FavoriteCounter