import { createBrowserRouter } from "react-router-dom";
import App from './App';
import Cart from "./components/Cart/Cart";
import Layout from "./Layout";
import { useOutletContext } from "react-router-dom";
import Favorite from "./components/Cart/Favorite";

export const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        index: true,
        element: (
          <AppWrapper />
        ),
      },
      {
        path: "/cart",
        element: <Cart/>
      },
      {
        path: "/favorite",
        element: <Favorite/>
      },
    ]
  },
]);

function AppWrapper() {
  const { isModalOpen, closeModal, openModal, setCartCount, setFavorite } =
    useOutletContext();

  return (
    <App
      isModalOpen={isModalOpen}
      closeModal={closeModal}
      openModal={openModal}
      setCartCount={setCartCount}
      setFavorite={setFavorite}
    />
  );
}

export default router;
