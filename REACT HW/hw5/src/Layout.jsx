import React, { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";

function Layout() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [cartItems, setCartItems] = useState([]);
  const [favorite, setFavorite] = useState(0);

  function openModal() {
    setIsModalOpen(true);
  }

  function closeModal() {
    setIsModalOpen(false);
  }

  useEffect(() => {
    const cartItemsFromStorage = JSON.parse(localStorage.getItem('cartItems')) || [];
    setCartItems(cartItemsFromStorage);
  }, []);

  return (
    <div>
      <Header cartCount={cartItems.length} favorite={favorite} />
      <div className="main-content">
        <Outlet
          context={{
            isModalOpen,
            closeModal,
            openModal,
            setCartItems,
            setFavorite,
            cartItems,
          }}
        />
      </div>
      <Footer />
    </div>
  );
}

export default Layout;
