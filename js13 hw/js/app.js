"use strict";
/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
Цей метод запобігає виконуванню стандартних дій браузеру, таких як перезід за посиланням, перезавантаження сторінки при кліку по кнопці відправки форми тощо.
2. В чому сенс прийому делегування подій?
Сенс полягає у обробці подій дочірніх елементів через батьківський. 
3. Які ви знаєте основні події документу та вікна браузера? 
DOMContentLoaded,load,beforeunload,unload

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

document.addEventListener('DOMContentLoaded', function () {

  const tabs = document.querySelector('.tabs');
  const tabsContent = document.querySelector('.tabs-content');

  tabs.addEventListener('click', function (e) {
    if (e.target.classList.contains('tabs-title')) {
      const activeTab = document.querySelector('.tabs-title.active');
      activeTab.classList.remove('active');
      e.target.classList.add('active');

      const elem = Array.from(tabs.children).indexOf(e.target);
      const activeContent = document.querySelector('.tabs-content .active');
      activeContent.classList.remove('active');
      tabsContent.children[elem].classList.add('active');
    }
  });
}
);