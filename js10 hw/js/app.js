"use strict";

// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

// Element.innerHTML 
// Element.innerText
// document.createTextNode
// document.createElement
// appendChild

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

// Спочатку використовуємо методи пошуку, наприклад querySelector, querySelectorAll, getElementsByClassName. 
// Далі за допомогою Element.remove видаляємо його.

// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

// myElement.parentNode.insertBefore
// myElement.parentNode.insertAfter

// 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

let myLink = document.createElement ('a');
myLink.textContent = 'Learn More';
myLink.href = "#";
let footerElement = document.querySelector ('footer');
let paragraphElement = document.getElementsByClassName ('p');
footerElement.insertBefore (myLink, paragraphElement.nextSibling);


// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

let ratingSelect = document.createElement('select');

ratingSelect.id = 'rating';

let optionFourStars = document.createElement('option');
optionFourStars.value = '4';
optionFourStars.textContent = '4 Stars';

let optionThreeStars = document.createElement('option');
optionThreeStars.value = '3';
optionThreeStars.textContent = '3 Stars';

let optionTwoStars = document.createElement('option');
optionTwoStars.value = '2';
optionTwoStars.textContent = '2 Stars';

let optionOneStar = document.createElement('option');
optionOneStar.value = '1';
optionOneStar.textContent = '1 Star';

ratingSelect.appendChild(optionFourStars);
ratingSelect.appendChild(optionThreeStars);
ratingSelect.appendChild(optionTwoStars);
ratingSelect.appendChild(optionOneStar);

let mainElement = document.querySelector('main');

let featuresSection = mainElement.querySelector('.features');

mainElement.insertBefore(ratingSelect, featuresSection);