"use strict";

// Теоретичні питання
// 1. Що таке логічний оператор?
// Логічні оператори - це елементи коду, котрі використовуються для вираження логічних відносин між значеннями чи виразами. 

// 2. Які логічні оператори є в JavaScript і які їх символи?

// "І" (&&), "АБО" (||), "НЕ" (!), "І" з присвоєнням (&&=), "АБО" з присвоєнням (||=), "НЕ" з присвоєнням (^=)

// Практичні завдання.
// 1. Попросіть користувача ввести свій вік за допомогою prompt.
// Якщо вік менше 12 років, виведіть у вікні alert повідомлення про те,
// що він є дитиною, якщо вік менше 18 років, виведіть повідомлення про те,
// що він є підлітком, інакше виведіть повідомлення про те, що він є дорослим.

// Завдання з підвищенною складністю.
// Перевірте чи ввів користувач саме число в поле prompt. Якщо ні, то виведіть повідомлення, що введено не число.

let ageInput = prompt("Будь ласка, введіть свій вік:");

if (!isNaN(ageInput)) {
  let age = parseInt(ageInput);

  if (age < 12) {
    alert("Ви є дитиною.");
  } else if (age < 18) {
    alert("Ви є підлітком.");
  } else {
    alert("Ви є дорослим.");
  }
} else {
  alert("Вказано не число. Перевірте коректність введення.");
}

// 2. Напишіть програму, яка запитує у користувача місяць року (українською мовою маленкими літерами) та виводить повідомлення,
// скільки днів у цьому місяці. Результат виводиться в консоль.
// Скільки днів в кожному місяці можна знайти тут в розділі Юліанський і Григоріанський календарі - https://uk.wikipedia.org/wiki/%D0%9C%D1%96%D1%81%D1%8F%D1%86%D1%8C_(%D1%87%D0%B0%D1%81)
// (Використайте switch case)

let month = prompt("Будь ласка, введіть місяць року (українською мовою):").toLowerCase();

let daysInMonth;

switch (month) {

    case "січень":
        daysInMonth = 31;
        break;

    case "лютий":
        daysInMonth = "28 або 29";
        break;

    case "березень":
        daysInMonth = 31;
        break;

    case "квітень":
        daysInMonth = 30;
        break;

    case "травень":
        daysInMonth = 31;
        break;

    case "червень":
        daysInMonth = 30;
        break;

    case "липень":
        daysInMonth = 31;
        break;

    case "серпень":
        daysInMonth = 31;
        break;

    case "вересень":
        daysInMonth = 30;
        break;
    case "жовтень":
        daysInMonth = 31;
        break;

    case "листопад":
        daysInMonth = 30;
        break;
    case "грудень":
        daysInMonth = 31;
        break;


    default:
        daysInMonth = "Невірно введений місяць";
}

console.log(`У місяці ${month} ${daysInMonth} день/днів.`);