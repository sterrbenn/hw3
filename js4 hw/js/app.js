"use strict";


// 1. Що таке цикл в програмуванні?


//  це конструкція, яка дозволяє виконувати один і той самий блок коду декілька разів

// 2. Які види циклів є в JavaScript і які їх ключові слова?

// Цикл for, Цикл while, Цикл do-while, Цикл for-in, Цикл for...of. Ключові слова відповідні

// 3. Чим відрізняється цикл do while від while?

// while спочатку перевіряє умову, а потім виконує цикл коду, а do while навпаки

// 1. Запитайте у користувача два числа.
// Перевірте, чи є кожне з введених значень числом.
// Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
// Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.

function isNumber(value) {
    return !isNaN(value) && Number.isFinite(value);
}

let userNumber1;
let userNumber2;

do {
    let input1 = prompt("Введіть перше число:");
    userNumber1 = parseFloat(input1);
}
while (!isNumber(userNumber1));

do {
    let input2 = prompt("Введіть друге число:");
    userNumber2 = parseFloat(input2);
}
while (!isNumber(userNumber2));

let startNumber = Math.min(userNumber1, userNumber2);
let endNumber = Math.max(userNumber1, userNumber2);

for (let i = startNumber; i <= endNumber; i++) {
    console.log(i);
}

// 2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.

let userInput;
let number;

do {
    userInput = prompt("Будь ласка, введіть парне число:");
    number = parseInt(userInput);

    if (isNaN(number) || number % 2!== 0) 

    {alert ('Введене число не є парним. Будь ласка, введіть парне число:')} }
    while (isNaN(number) || number % 2 !== 0) 
