'use strict';

// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Асинхронність у js - це спосіб виконання коду не у порядку його написання.
// Полягає у використанні методів, які "змушують" функції чекати на якісь події, не зупиняючи відтворення решти коду.

// Завдання
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const btn = document.getElementById('IP');

async function getIP() {
    try {
        const URL = "https://api.ipify.org/?format=json";
        const response = await fetch(URL);
        if (!response.ok) { throw new Error('No Response') };
        const data = await response.json();
        return data.ip;
    } catch (error) {
        console.log('No response', error);
        throw error;
    }
}

async function getIPDetails() {
    try {
        const URL = 'http://ip-api.com/json/?fields=continent,country,regionName,city,district';
        const response = await fetch(URL);
        if (!response.ok) { throw new Error('No Response') };
        const data = await response.json();
        return data;
    } catch (error) {
        console.log('Error:', error);
        throw error;
    }
}

btn.addEventListener('click', async () => {
    try {
        const userIP = await getIP();
        console.log(`IP-адреса: ${userIP}`);
        const userDetails = await getIPDetails(userIP);
        console.log('Місце знаходження користувача:', userDetails);
        const root = document.getElementById('root');
        root.innerHTML=`
        <p>Континент:${userDetails.continent}</p>
        <p>Країна:${userDetails.country}</p>
        <p>Регіон:${userDetails.regionName}</p>
        <p>Місто:${userDetails.city}</p>
        <p>Район:<${userDetails.district}/p>`;
    } catch (error) {
        console.log('Не вдалося отримати інформацію про місце знаходження користувача:', error);
    }
});



