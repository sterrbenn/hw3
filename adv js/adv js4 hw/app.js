'use strict';

// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// Це технологія, котра дозволяє клієнту взаємодіяти з сервером та не перезавантажувати при цьому сторінку. 
// Таким чином вміст може оновлюватися динамічно, і у процесі оновлення також отримувати нові дані із сервера.
// Технологія AJAX базується на асинхронному виконанні коду, тобто операції виконуються незалежно одна від одної, 
// без очікування завершення попередніх операцій. Це означає, що код може продовжувати виконуватися, навіть якщо попередні операції ще не були виконані.

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.


const URL = 'https://ajax.test-danit.com/api/swapi/films';

class Request {
    static get(url) {
        return fetch(url)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network Error');
                }
                return response.json();
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }
}


class StarWars {
    async render(data) {
        let ul = document.createElement('ul');

        for (const film of data) {
            let li = document.createElement('li');
            li.textContent = `Name of the film: ${film.name}, Episode: ${film.episodeId}, Opening Crawl: ${film.openingCrawl}`;

            let charactersData = await Promise.all(film.characters.map(characterURL => {
                return Request.get(characterURL);
            }));

            let characterNames = charactersData.map(character => character.name).join(', ');
            let charactersList = document.createElement('ul');
            charactersList.innerHTML = `<li>Characters: ${characterNames}</li>`;
            li.appendChild(charactersList);

            ul.appendChild(li);
            console.log(data);
        }

        return ul;
    }
}

const div = document.querySelector('div');
const starWars = new StarWars();

Request.get(URL)
    .then(films => {
        starWars.render(films).then(ul => {
            div.appendChild(ul);
        });
    })
    .catch(error => {
        console.error('Error fetching films:', error);
    });
