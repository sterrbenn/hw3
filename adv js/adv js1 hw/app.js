'use strict';

// Теоретичне питання
// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript 

// Прототипне наслідування працює таким чином, що якщо властивість не була знайдена у об'єкту, то js шукає його у властивості prototype,
//  в яку можна присвоїти інший об'єкт зі своїми вже існуючими властивостями.

// Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Щоб запустити батьківський конструктор

// Завдання
// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {

        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
};

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get programmerSalary () {
        return this._salary*3;
    }
};

const programmerJohn = new Programmer ('John', 25, 2000, 'C#');
console.log(programmerJohn);

const programmerGeorge = new Programmer ('George', 23, 3500, 'C');
console.log(programmerGeorge);

const programmerPaul = new Programmer ('Paul', 24, 2500, 'JavaScript');
console.log(programmerPaul);

const programmerRingo = new Programmer ('Ringo', 26, 2150, 'Python');
console.log(programmerRingo);

