'use strict';

// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад доданий в репозиторії групи, посилання на ДЗ вище), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.


const usersURL = 'https://ajax.test-danit.com/api/json/users';
const postsURL = 'https://ajax.test-danit.com/api/json/posts';


class DataFetcher {
    async fetchData(url) {
        try {
            const response = await fetch(url);
            if (!response.ok) {
                throw new Error('Bad network');
            }
            const data = await response.json();
            return data;
        } catch (error) {
            console.error('Error:', error);
            throw error;
        }
    }
}

class PostCard {

    constructor(post, user) {
        this.post = post;
        this.user = user;
    }

    createCard() {
        const card = document.createElement('div');
        card.classList.add('post-card');
        card.style.border = '10px solid blue';
        card.style.borderRadius = '10px';
        card.style.margin = '10px';
        card.style.boxSizing = 'border-box';
        card.style.padding = '10px';
    
        const title = document.createElement('h2');
        title.textContent = `${this.user.name}, ${this.user.email}`;
    
        const postTitle = document.createElement('h3');
        postTitle.textContent = this.post.title;
    
        const text = document.createElement('p');
        text.textContent = this.post.body;
    
        const deleteButton = document.createElement('button');
        deleteButton.classList.add('delete-btn');
        deleteButton.textContent = 'Delete'; 
        deleteButton.addEventListener('click', () => {
            this.deleteCard();
        });
    
        card.appendChild(title);
        card.appendChild(postTitle);
        card.appendChild(text);
        card.appendChild(deleteButton); 
    
        const postID = this.post.id;
        const cardID = `post-${postID}`;
        card.id = cardID;
    
        return card;
    }

    deleteCard() {
        const postID = this.post.id;
        const cardID = `post-${postID}`;
        const deleteURL = `https://ajax.test-danit.com/api/json/posts/${postID}`;
    
        fetch(deleteURL, {
            method: 'DELETE'
        }).then(response => {
            if (response.ok) {
                const card = document.getElementById(cardID);
                if (card) {
                    card.parentNode.removeChild(card);
                } else {
                    console.log('Card not found:', cardID);
                }
            } else {
                throw new Error('Failed to delete post');
            }
        }).catch(error => {
            console.log('Failed to delete post:', error);
        });
    }
}

class PostDisplayer {
    constructor(rootElement) {
        this.rootElement = rootElement;
    }

    async displayPosts(posts) {
        const dataFetcher = new DataFetcher();
        const root = document.querySelector(this.rootElement);

        posts.forEach(async post => {
            try {
                const user = await dataFetcher.fetchData(`${usersURL}/${post.userId}`);
                const postCard = new PostCard(post, user);
                root.appendChild(postCard.createCard());
            } catch (error) {
                console.log('Error fetching user for post:', error);
            }
        });
    }
}

const dataFetcher = new DataFetcher();
const postDisplayer = new PostDisplayer('#root');

dataFetcher.fetchData(postsURL)
    .then(posts => {
        postDisplayer.displayPosts(posts);
    })
    .catch(error => {
        console.log('Error fetching posts:', error);
    });


dataFetcher.fetchData(usersURL)
    .then(users => {
        console.log('Users', users);
    })
    .catch(error => {
        console.log('Error fetching users:', error);
    });

