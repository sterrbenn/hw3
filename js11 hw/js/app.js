"use strict";

// 1. Що таке події в JavaScript і для чого вони використовуються?
// Події у JS - це моменти взаємодії користувача зі сторінкою браузера. Використовуются вони для того, щоб у момент взаємодії якимось чином змінювати вміст вікна
// 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
// click,mouseover,mouseout,scroll
// 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
// Це подія, що відбувається при натисканні ПКМ по об'єкту. Використовується для того, щоб вивести замість стандартного меню наше власне


// 1. Додати новий абзац по кліку на кнопку:
// По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">

let btn = document.getElementById('btn-click');

btn.addEventListener('click', function () {

    const newParagraph = document.createElement('p');
    newParagraph.textContent = 'New Paragraph';
    const contentSection = document.getElementById('content');
    contentSection.appendChild(newParagraph);
},{ once: true });


// 2. Додати новий елемент форми із атрибутами:
// Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
// По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.

const buttonInputCreate = document.createElement('button');
buttonInputCreate.id = 'button-input-create';
buttonInputCreate.textContent = 'Button';

const contentSection = document.getElementById('content');
const footer = document.querySelector('footer');

buttonInputCreate.addEventListener('click', function () {
    const createdInput = document.createElement('input');
    createdInput.setAttribute('name', 'userInput');
    createdInput.setAttribute('type', 'text');
    createdInput.setAttribute('placeholder', 'Введіть ваше повідомлення');
    contentSection.parentNode.insertBefore(createdInput, footer);
   
}, { once: true } );

contentSection.parentNode.insertBefore(buttonInputCreate, footer);

// Знайти інфу як зробити у js, щоб поле з'являлося під кнопкою, а не праворуч, не зміг