"use strict";

const DATA = [
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m1.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "8 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f1.png",
		specialization: "Тренажерний зал",
		category: "Спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м`язової маси. Її клієнти завжди задоволені результатами. ",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m2.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Тетяна",
		"last name": "Мороз",
		photo: "./img/trainers/trainer-f2.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Тетяна є експертом в бойових мистецтвах. Вона проводить тренування для професіоналів і новачків. Її підхід до навчання допомагає спортсменам досягати високих результатів.",
	},
	{
		"first name": "Сергій",
		"last name": "Коваленко",
		photo: "./img/trainers/trainer-m3.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Сергій фокусується на роботі з фітнесом та кардіотренуваннями. Він вдосконалив свої методики протягом багатьох років. Його учні завжди в формі та енергійні.",
	},
	{
		"first name": "Олена",
		"last name": "Лисенко",
		photo: "./img/trainers/trainer-f3.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Олена спеціалізується на синхронному плаванні. Вона тренує команди різного рівня. Її команди завжди займають призові місця на змаганнях.",
	},
	{
		"first name": "Андрій",
		"last name": "Волков",
		photo: "./img/trainers/trainer-m4.jpg",
		specialization: "Бійцівський клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій має досвід у вивченні різних бойових мистецтв. Він викладає техніку та тактику бою. Його учні здобувають перемоги на міжнародних турнірах.",
	},
	{
		"first name": "Наталія",
		"last name": "Романенко",
		photo: "./img/trainers/trainer-f4.jpg",
		specialization: "Дитячий клуб",
		category: "спеціаліст",
		experience: "3 роки",
		description:
			"Наталія розробила унікальну програму для дітей дошкільного віку. Вона допомагає дітям розвивати фізичні та ментальні навички. Її класи завжди веселі та динамічні.",
	},
	{
		"first name": "Віталій",
		"last name": "Козлов",
		photo: "./img/trainers/trainer-m5.jpg",
		specialization: "Тренажерний зал",
		category: "майстер",
		experience: "10 років",
		description:
			"Віталій спеціалізується на функціональному тренуванні. Він розробив ряд ефективних тренувальних програм. Його клієнти швидко досягають бажаних результатів.",
	},
	{
		"first name": "Юлія",
		"last name": "Кравченко",
		photo: "./img/trainers/trainer-f5.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "4 роки",
		description:
			"Юлія є експертом у водних видах спорту. Вона проводить тренування з аквагімнастики та аеробіки. Її учні демонструють вражаючі показники на змаганнях.",
	},
	{
		"first name": "Олег",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-m6.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "12 років",
		description:
			"Олег є визнаним майстром в бойових мистецтвах. Він тренує чемпіонів різних вагових категорій. Його методики вважаються одними з найефективніших у світі бойових мистецтв.",
	},
	{
		"first name": "Лідія",
		"last name": "Попова",
		photo: "./img/trainers/trainer-f6.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Лідія має великий досвід у роботі з дітьми. Вона організовує різноманітні спортивні ігри та заняття. Її класи завжди допомагають дітям розвивати соціальні навички та командний дух.",
	},
	{
		"first name": "Роман",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m7.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Роман є експертом у кросфіту та функціональних тренуваннях. Він розробив власні програми для різних вікових груп. Його учні часто отримують нагороди на фітнес-турнірах.",
	},
	{
		"first name": "Анастасія",
		"last name": "Гончарова",
		photo: "./img/trainers/trainer-f7.jpg",
		specialization: "Басейн",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Анастасія фокусується на водних програмах для здоров`я та фітнесу. Вона проводить тренування для осіб з різним рівнем підготовки. Її учні відзначають покращення здоров`я та благополуччя після занять.",
	},
	{
		"first name": "Валентин",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-m8.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Валентин є експертом в таеквондо та кікбоксингу. Він викладає техніку, тактику та стратегію бою. Його учні часто стають чемпіонами на національних та міжнародних змаганнях.",
	},
	{
		"first name": "Лариса",
		"last name": "Петренко",
		photo: "./img/trainers/trainer-f8.jpg",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "7 років",
		description:
			"Лариса розробила комплексну програму для розвитку фізичних та інтелектуальних навичок дітей. Вона проводить заняття в ігровій формі. Її методика допомагає дітям стати активними та розумними.",
	},
	{
		"first name": "Олексій",
		"last name": "Петров",
		photo: "./img/trainers/trainer-m9.jpg",
		specialization: "Басейн",
		category: "майстер",
		experience: "11 років",
		description:
			"Олексій має багаторічний досвід роботи з плавцями. Він займається якісною підготовкою спортсменів на міжнародних змаганнях. Його методика базується на новітніх технологіях тренувань.",
	},
	{
		"first name": "Марина",
		"last name": "Іванова",
		photo: "./img/trainers/trainer-f9.jpg",
		specialization: "Тренажерний зал",
		category: "спеціаліст",
		experience: "2 роки",
		description:
			"Марина спеціалізується на роботі з ваговими тренажерами. Вона розробила унікальну програму для набору м`язової маси. Її клієнти завжди задоволені результатами.",
	},
	{
		"first name": "Ігор",
		"last name": "Сидоренко",
		photo: "./img/trainers/trainer-m10.jpg",
		specialization: "Дитячий клуб",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Ігор працює з дітьми різного віку. Він створив ігрові методики для розвитку координації та спритності. Його уроки завжди цікаві та корисні для малюків.",
	},
	{
		"first name": "Наталія",
		"last name": "Бондаренко",
		photo: "./img/trainers/trainer-f10.jpg",
		specialization: "Бійцівський клуб",
		category: "майстер",
		experience: "8 років",
		description:
			"Наталія є майстром у бойових мистецтвах. Вона вивчала різні техніки та стили, із якими працює зі своїми учнями. Її підхід до навчання відповідає найвищим стандартам.",
	},
	{
		"first name": "Андрій",
		"last name": "Семенов",
		photo: "./img/trainers/trainer-m11.jpg",
		specialization: "Тренажерний зал",
		category: "інструктор",
		experience: "1 рік",
		description:
			"Андрій спеціалізується на функціональному тренуванні. Він розробив власну систему вправ для зміцнення корпусу. Його учні завжди отримують видимі результати.",
	},
	{
		"first name": "Софія",
		"last name": "Мельник",
		photo: "./img/trainers/trainer-f11.jpg",
		specialization: "Басейн",
		category: "спеціаліст",
		experience: "6 років",
		description:
			"Софія працює з аквагімнастикою. Вона вивчила різні техніки та стили плавання. Її заняття допомагають клієнтам розслабитися та покращити фізичну форму.",
	},
	{
		"first name": "Дмитро",
		"last name": "Ковальчук",
		photo: "./img/trainers/trainer-m12.png",
		specialization: "Дитячий клуб",
		category: "майстер",
		experience: "10 років",
		description:
			"Дмитро спеціалізується на розвитку дитячого спорту. Він розробив унікальну програму для малюків. Його методики забезпечують гармонійний розвиток дітей.",
	},
	{
		"first name": "Олена",
		"last name": "Ткаченко",
		photo: "./img/trainers/trainer-f12.jpg",
		specialization: "Бійцівський клуб",
		category: "спеціаліст",
		experience: "5 років",
		description:
			"Олена є відомим тренером у жіночому бойовому клубі. Вона вивчила різні техніки самооборони. Її підхід дозволяє її ученицям відчувати себе впевнено в будь-яких ситуаціях.",
	},
];

const trainersContainer = document.querySelector('.trainers-cards__container');

DATA.forEach((trainer) => {
	const trainerCard = document.createElement('li');
	trainerCard.classList.add('trainer');

	trainerCard.innerHTML = `
    <img src="${trainer.photo}"
        alt="trainer photo"
        class="trainer__img"
        width="280"
        height="300" /> 
    <p class="trainer__name">${trainer['first name']} ${trainer['last name']}</p>
    <button class="trainer__show-more" type="button" data-trainer='${JSON.stringify(trainer)}'>ПОКАЗАТИ</button>`;

	trainersContainer.appendChild(trainerCard);
});

let defaultArray = DATA.slice();

const sideMenu = document.querySelector('.sidebar');
sideMenu.removeAttribute("hidden");
const sortingMenu = document.querySelector('.sorting');
sortingMenu.removeAttribute("hidden");

const modalTemplate = document.getElementById('modal-template');

function openModal(trainer) {

	const modalContent = modalTemplate.content.cloneNode(true);

	const modalImg = modalContent.querySelector('.modal__img');
	const modalName = modalContent.querySelector('.modal__name');
	const modalCategory = modalContent.querySelector('.modal__point--category');
	const modalExperience = modalContent.querySelector('.modal__point--experience');
	const modalSpecialization = modalContent.querySelector('.modal__point--specialization');
	const modalTrainerInfo = modalContent.querySelector('.modal__text');

	modalImg.src = trainer.photo;
	modalImg.alt = `${trainer['first name']} ${trainer['last name']}`;
	modalName.innerHTML = `${trainer['first name']} ${trainer['last name']}`;
	modalCategory.innerHTML = `Категорія: ${trainer.category}`;
	modalExperience.innerHTML = `Досвід: ${trainer.experience}`;
	modalSpecialization.innerHTML = `Напрям тренера: ${trainer.specialization}`;
	modalTrainerInfo.innerHTML = trainer.description;

	const closeModalButton = modalContent.querySelector('.modal__close');
	closeModalButton.addEventListener('click', closeModal);

	document.body.style.overflow = "hidden";

	document.body.appendChild(modalContent);

}

document.querySelectorAll('.trainer__show-more').forEach(button => {
	button.addEventListener('click', function () {
		const trainer = JSON.parse(this.dataset.trainer);
		openModal(trainer);
	})
});

function closeModal() {
	const modal = document.querySelector('.modal');
	modal.remove();
	document.body.style.overflow = "";
};

function sortingButtons() {
	const sortingBtns = document.querySelectorAll('.sorting__btn');

	sortingBtns.forEach(btn => {
		btn.addEventListener('click', function () {
			sortingBtns.forEach(btn => (
				btn.classList.remove('sorting__btn--active')
			));

			btn.classList.add('sorting__btn--active');
		})
	})
};
sortingButtons();


function sortingByName(a, b) {
	let nameA = a["last name"].toLowerCase(), nameB = b["last name"].toLowerCase();
	if (nameA < nameB)
		return -1;
	if (nameA > nameB)
		return 1;
	return 0;
}

function showSortedData1(sortedData) {
	trainersContainer.innerHTML = '';

	sortedData.forEach((trainer) => {
		const trainerCard = document.createElement('li');
		trainerCard.classList.add('trainer');

		trainerCard.innerHTML = `
            <img src="${trainer.photo}"
                alt="trainer photo"
                class="trainer__img"
                width="280"
                height="300" /> 
            <p class="trainer__name">${trainer['first name']} ${trainer['last name']}</p>
            <button class="trainer__show-more" type="button" data-trainer='${JSON.stringify(trainer)}'>ПОКАЗАТИ</button>`;

		trainersContainer.appendChild(trainerCard);

		trainerCard.querySelector('.trainer__show-more').addEventListener('click', function () {
			const trainer = JSON.parse(this.dataset.trainer);
			openModal(trainer);
		});
	});
}

const lastNameSorting = document.querySelector('.sorting__btn:nth-of-type(2)');

lastNameSorting.setAttribute('id', 'lastNameSorting');

const activeBtn1 = document.getElementById('lastNameSorting');

activeBtn1.addEventListener('click', function () {
	DATA.sort(sortingByName);
	showSortedData1(DATA);
})


function sortingByExperience(a, b) {
	let experienceA = parseInt(a.experience), experienceB = parseInt(b.experience);
	if (experienceA < experienceB)
		return 1;
	if (experienceA > experienceB)
		return -1;
	return 0;
};
function showSortedData2(sortedData) {
	trainersContainer.innerHTML = '';

	sortedData.forEach((trainer) => {
		const trainerCard = document.createElement('li');
		trainerCard.classList.add('trainer');

		trainerCard.innerHTML = `
            <img src="${trainer.photo}"
                alt="trainer photo"
                class="trainer__img"
                width="280"
                height="300" /> 
            <p class="trainer__name">${trainer['first name']} ${trainer['last name']}</p>
            <button class="trainer__show-more" type="button" data-trainer='${JSON.stringify(trainer)}'>ПОКАЗАТИ</button>`;

		trainersContainer.appendChild(trainerCard);

		trainerCard.querySelector('.trainer__show-more').addEventListener('click', function () {
			const trainer = JSON.parse(this.dataset.trainer);
			openModal(trainer);
		});
	});
}

const experienceSorting = document.querySelector('.sorting__btn:nth-of-type(3)');

experienceSorting.setAttribute('id', 'experienceSorting');

const activeBtn2 = document.getElementById('experienceSorting');

activeBtn2.addEventListener('click', function () {
	DATA.sort(sortingByExperience);
	showSortedData2(DATA);
});

const defaultSorting = document.querySelector('.sorting__btn:nth-of-type(1)');

defaultSorting.addEventListener('click', function () {
	showSortedData1(defaultArray);
})


let poolArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Басейн')
		poolArr.push(trainer)
}

let fightArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Бійцівський клуб')
		fightArr.push(trainer)
}
let childrenArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Дитячий клуб')
		childrenArr.push(trainer)
}

let gymArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Тренажерний зал')
		gymArr.push(trainer)
}

let masterArr = [];
for (const trainer of DATA) {
	if (trainer.category === 'майстер')
		masterArr.push(trainer)
}

let specialistArr = [];
for (const trainer of DATA) {
	if (trainer.category === 'спеціаліст')
		specialistArr.push(trainer)
}

let InstructorArr = [];
for (const trainer of DATA) {
	if (trainer.category === 'інструктор')
		InstructorArr.push(trainer)
}

let gymMasterArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Тренажерний зал' && trainer.category === 'майстер')
		gymMasterArr.push(trainer)
}

let gymSpecialistArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Тренажерний зал' && trainer.category === 'спеціаліст')
		gymSpecialistArr.push(trainer)
}

let gymInstructorArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Тренажерний зал' && trainer.category === 'інструктор')
		gymInstructorArr.push(trainer)
}

let fightMasterArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Бійцівський клуб' && trainer.category === 'майстер')
		fightMasterArr.push(trainer)
}

let fightSpecialisArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Бійцівський клуб' && trainer.category === 'спеціаліст')
		fightSpecialisArr.push(trainer)
}

let fightInstructorArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Бійцівський клуб' && trainer.category === 'інструктор')
		fightInstructorArr.push(trainer)
}

let childrenMasterArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Дитячий клуб' && trainer.category === 'майстер')
		childrenMasterArr.push(trainer)
}

let childrenSpecialistArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Дитячий клуб' && trainer.category === 'спеціаліст')
		childrenSpecialistArr.push(trainer)
}

let childrenInstructorArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Дитячий клуб' && trainer.category === 'інструктор')
		childrenInstructorArr.push(trainer)
}

let swimmingMasterArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Басейн' && trainer.category === 'майстер')
		swimmingMasterArr.push(trainer)
}

let swimmingSpecialisArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Басейн' && trainer.category === 'спеціаліст')
		swimmingSpecialisArr.push(trainer)
}

let swimmingInstructorArr = [];
for (const trainer of DATA) {
	if (trainer.specialization === 'Басейн' && trainer.category === 'інструктор')
		swimmingInstructorArr.push(trainer)
}


const showButton = document.querySelector('.filters__submit');

showButton.addEventListener('click', function (event) {
	event.preventDefault();

	const selectedDirection = document.querySelector('input[name="direction"]:checked').value;
	const selectedCategory = document.querySelector('input[name="category"]:checked').value;

	if (selectedDirection === 'all' && selectedCategory === 'all') {
		showSortedData2(DATA);
	} else if (selectedDirection === 'swimming pool' && selectedCategory === 'master') {
		showSortedData2(swimmingMasterArr);
	} else if (selectedDirection === 'swimming pool' && selectedCategory === 'specialist') {
		showSortedData2(swimmingSpecialisArr);
	} else if (selectedDirection === 'swimming pool' && selectedCategory === 'instructor') {
		showSortedData2(swimmingInstructorArr);
	} else if (selectedDirection === 'swimming pool') {
		showSortedData2(poolArr);
	} else if (selectedDirection === 'kids club' && selectedCategory === 'master') {
		showSortedData2(childrenMasterArr);
	} else if (selectedDirection === 'kids club' && selectedCategory === 'specialist') {
		showSortedData2(childrenSpecialistArr);
	} else if (selectedDirection === 'kids club' && selectedCategory === 'instructor') {
		showSortedData2(childrenInstructorArr);
	} else if (selectedDirection === 'kids club') {
		showSortedData2(childrenArr);
	} else if (selectedDirection === 'fight club' && selectedCategory === 'master') {
		showSortedData2(fightMasterArr);
	} else if (selectedDirection === 'fight club' && selectedCategory === 'specialist') {
		showSortedData2(fightSpecialisArr);
	} else if (selectedDirection === 'fight club' && selectedCategory === 'instructor') {
		showSortedData2(fightInstructorArr);
	} else if (selectedDirection === 'fight club') {
		showSortedData2(fightArr);
	} else if (selectedDirection === 'gym' && selectedCategory === 'master') {
		showSortedData2(gymMasterArr);
	} else if (selectedDirection === 'gym' && selectedCategory === 'specialist') {
		showSortedData2(gymSpecialistArr);
	} else if (selectedDirection === 'gym' && selectedCategory === 'instructor') {
		showSortedData2(gymInstructorArr);
	} else if (selectedDirection === 'gym') {
		showSortedData2(gymArr);
	} else if (selectedCategory === 'master') {
		showSortedData2(masterArr);
	} else if (selectedCategory === 'specialist') {
		showSortedData2(specialistArr);
	} else if (selectedCategory === 'instructor') {
		showSortedData2(InstructorArr);
	}
});

