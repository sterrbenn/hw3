"use strict";

// 1. В чому відмінність між setInterval та setTimeout?
// setInterval виконує функцію постійно через заданий проміжок часу, у той час як setTimeout виконує лише один раз

// 2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
// Ні, не можна, бо усе залежить від потужності девайсу, на якому запускається браузер, а також від ішних чинників, таких як завантаженість браузеру, стан системи тощо 
// 3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
// За допомогою clearTimeout

// Практичне завдання:
// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

const button = document.querySelector('.button');
const text = document.querySelector('.text');

button.addEventListener('click', function () {
    setTimeout(function () { text.textContent = "I was changed!" }, 3000)
})

// Практичне завдання 2:
// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div.
//  Після досягнення 1 змініть текст на "Зворотній відлік завершено".

const Arr = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

let i = 0;
const intervalTimer = setInterval(function () {
    if (i < Arr.length) {
        const element = Arr[i];
        console.log(element);
        i++;
    } else {
        clearInterval(intervalTimer);
        const alert = ('Зворотній відлік завершено');
        console.log(alert);
    }
}, 1000);

