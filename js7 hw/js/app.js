"use strict";

// Теоретичні питання
// 1. Як можна створити рядок у JavaScript?
// Рядок можна створити за допомогою одинарних, подвійних, зворотніх лапок, або конструктора String
// 2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
// Одинарні та подвійні лапки є взаємозмінними, та функціонально не відрізняються одні від одних, 
// а зворотні лапки формують шаблонний рядок, у який можно через конструкцію ${} додавати вирази інших видів даніх
// 3. Як перевірити, чи два рядки рівні між собою?
// За допомогою операторів порівняння == , != , === , !== , <= , >= 
// 4. Що повертає Date.now()?
// Кількість мілісекунд, які сплинули від 1 січня 1970 року 
// 5. Чим відрізняється Date.now() від new Date()?
// Date.now () повертає кількість мілісекунд, які сплинули від 1 січня 1970 року, 
// у той час як new Date створює об'єкт типу Date, який повертає поточну дату у стандартному вигляді (день тижня, місяць, число, рік, час)

// 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
// (читається однаково зліва направо і справа наліво), або false в іншому випадку.

function isPalindrome(str) {
    return str === str.split('').reverse().join('');
}

let result1 = isPalindrome('abcdedcba');
console.log(result1);

let result2 = isPalindrome('abcdabcd');
console.log(result2);

// 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true,
// якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми.
// Приклади використання функції:
// Рядок коротше 20 символів
// funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
// funcName('checked string', 10); // false

function checkStringLenght(str, maxStrLength) {
    return str.length <= maxStrLength;
}

let test1 = checkStringLenght('Hello', 8);
console.log(test1); 

let test2 = checkStringLenght('Hello', 3);
console.log(test2);

// 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. 
// Функція повина повертати значення повних років на дату виклику функцію.

function calculateAge(birthDate) {
    let currentDate = new Date();
    let birthDateObj = new Date(birthDate)
  
    let age = currentDate.getFullYear() - birthDateObj.getFullYear();
    if (
      currentDate.getMonth() < birthDateObj.getMonth() ||
      (currentDate.getMonth() === birthDateObj.getMonth() &&
        currentDate.getDate() < birthDateObj.getDate())
    ) {
      age--;
    }
    return age;
  }
  let userBirthDate = prompt('Будь ласка, введіть свою дату народження (рррр-мм-дд):');
  
  if (userBirthDate) {
    let userAge = calculateAge(userBirthDate);
    alert(`Вам ${userAge} років/ки.`);
  } else {
    alert('Введено невірну дату народження.');
  }