"use strict";


/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?

localStorage - це "довгостроковий" кеш, тобто зберігається до видалення його з браузера вручну. sessionStorage - це кеш поточної сесії. Після закриття 
вкладки або браузеру він видяляється. 

2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?

Доступ до даних, збережених у localStorage можна отримати навіть після перезавантаження ОС.
Також дані, збережені в localStorage, доступні для всіх відкритих вкладок з тією ж самою доменною адресою.

3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

Видаляються

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-5 "Book Shop" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/


const button = document.createElement('button');
button.textContent = 'Змінити тему';
button.id = "changeThemeBtn";
const btnPlace = document.querySelector('.top-logo');
btnPlace.appendChild(button);

const pageTheme = localStorage.getItem ('theme');
if (pageTheme) {
  document.body.classList.add (pageTheme);
}

button.addEventListener('click', function() {
    const body = document.body;
  
    if (body.classList.contains('dark-mode')) {
        body.classList.remove('dark-mode');
        localStorage.setItem('theme', '');
    } else {
        body.classList.add('dark-mode');
        localStorage.setItem('theme', 'dark-mode')
    }
});

