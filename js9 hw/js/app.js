"use strict";

// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// Це модель опрацювання і виводу браузером html документу, яка представляє його у вигляді дерева об'єктів з вузлами розгалуження
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML вставляє код всередині html елемента і обробляє його так само, як html. innerText робить те ж саме, але не обробляє його як html, а просто показує текстовий вміст
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Методи кращі або не кращі у залежності від потреби:
// getElementsByClassName
// getElementsByTagName
// getElementById (не рекомендований спосіб)
// querySelector
// querySelectorAll
// 4. Яка різниця між nodeList та HTMLCollection?
// Принципова різниця полягає у тому, що HTMLCollection містить у собі тільки HTMLAllCollection, у той час як NodeList може містити будь які вузли документу

// 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
// Використайте 2 способи для пошуку елементів.
// Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

const elementsByClassName = document.getElementsByClassName('feature');

const elementsArrayByClassName = Array.from(elementsByClassName);

console.log(elementsArrayByClassName);

elementsArrayByClassName.forEach(element => {
    element.style.textAlign = 'center';
});

const elementsByQuerySelector = document.querySelectorAll('.feature');

const elementsArrayByQuerySelector = Array.from(elementsByQuerySelector);

console.log(elementsArrayByQuerySelector);

elementsArrayByQuerySelector.forEach(element => {
    element.style.textAlign = 'center';
});

// 2. Змініть текст усіх елементів h2 на "Awesome feature".

const titleElements = document.querySelectorAll('h2');

titleElements.forEach(element => {
    element.innerHTML = 'Awesome feature';
});

// 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

const titleFeature = document.getElementsByClassName("feature-title");

const featureArray = Array.from(titleFeature);

for (let i = 0; i < featureArray.length; i++) {
    featureArray[i].textContent += '!';
}